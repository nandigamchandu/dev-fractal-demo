import React from 'react'

import { Hello } from './Hello'
import { BasicForm } from './Form_Basic'
import { DevFractalForm } from './FormWithDev'
import { FormikDemo } from './Formik_Demo'
import { FormikDemo2 } from './Formik_Demo2'

export const App = () => (
  <>
    <p>==========Basic Form===================</p>
    <BasicForm
      onSubmit={values => {
        alert(JSON.stringify(values))
      }}
    />
    <p>==========Using formik=================</p>
    <FormikDemo />
    <p>==========Using formik2================</p>
    <FormikDemo2 />
    <p>==========Using dev-fractal============</p>
    <DevFractalForm />
  </>
)
