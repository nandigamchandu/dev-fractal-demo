import { Simple } from 'technoidentity-devfractal'
import React from 'react'
import * as Yup from 'yup'

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .email('E-mail is not valid')
    .required('E-mail is required'),
  password: Yup.string()
    .min(6, 'Password has to be longer than 6 characters!')
    .required('Password is required'),
  confirmPassword: Yup.string()
    .required('Confirm your password')
    .oneOf([Yup.ref('password')], 'Password does not match'),
})

export const DevFractalForm = () => {
  const initialValues = { email: '', password: '', confirmPassword: '' }
  return (
    <Simple.Form
      onSubmit={values => {
        alert(JSON.stringify(values))
      }}
      initialValues={initialValues}
      validationSchema={validationSchema}
    >
      <Simple.Text
        name="email"
        label="Email"
        // validates={[required(), maxLength(6)]}
      />
      <Simple.Password name="password" />
      <Simple.Password name="confirmPassword" />
      <Simple.FormButtons submit="Submit" reset={false} />
    </Simple.Form>
  )
}
