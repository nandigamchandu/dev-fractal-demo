import React from 'react'

interface BasicFormFields {
  email: string
  password: string
  confirmedPassword: string
}

interface BasicForm {
  onSubmit: (values: BasicFormFields) => void
}

export const BasicForm: React.FC<BasicForm> = ({ onSubmit }) => {
  const [state, setState] = React.useState<BasicFormFields>({
    email: '',
    password: '',
    confirmedPassword: '',
  })

  const [error, setError] = React.useState<BasicFormFields>({
    email: '',
    password: '',
    confirmedPassword: '',
  })

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [event.target.name]: event.target.value })
  }

  const handleSubmit = (event: any) => {
    onSubmit(state)
    event.preventDefault()
  }

  return (
    <>
      <form onSubmit={handleSubmit}>
        <label htmlFor="email">Email:</label>
        <input
          id="email"
          type="email"
          name="email"
          value={state.email}
          onChange={handleChange}
          onBlur={event => {
            state.email === ''
              ? setError({
                  ...error,
                  [event.target.name]: 'Required field',
                })
              : setError({
                  ...error,
                  [event.target.name]: '',
                })
          }}
        />
        <div>{error.email}</div>
        <label htmlFor="password">Password:</label>
        <input
          id="password"
          type="password"
          name="password"
          value={state.password}
          onChange={handleChange}
        />
        <div>{error.password}</div>
        <label htmlFor="confirmedPassword">Confirmed Password:</label>
        <input
          id="confirmedPassword"
          type="password"
          name="confirmedPassword"
          value={state.confirmedPassword}
          onChange={event => {
            handleChange(event)
          }}
          onBlur={event => {
            event.target.value !== state.password
              ? setError({
                  ...error,
                  [event.target.name]: 'password mismatch',
                })
              : setError({ ...error, [event.target.name]: '' })
          }}
        />
        <div>{error.confirmedPassword}</div>
        <button
          type="submit"
          disabled={
            error.email || error.password || error.confirmedPassword
              ? true
              : false
          }
        >
          Submit
        </button>
      </form>
    </>
  )
}
