import React from 'react'
import { Formik, FormikProps } from 'formik'
import * as Yup from 'yup'

interface FormValues {
  email: string
  password: string
  confirmPassword: string
}

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .email('E-mail is not valid')
    .required('E-mail is required'),
  password: Yup.string()
    .min(6, 'Password has to be longer than 6 characters!')
    .required('Password is required'),
  confirmPassword: Yup.string()
    .required('Confirm your password')
    .oneOf([Yup.ref('password')], 'Password does not match'),
})

const FormInner: React.FunctionComponent<FormikProps<FormValues>> = props => {
  const { errors, handleChange, handleSubmit } = props
  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="email">
        <span>Email:</span>
        <input name="email" type="email" onChange={handleChange} />
      </label>
      <div>{errors.email}</div>
      <label htmlFor="password">
        <span>password:</span>
        <input name="password" type="password" onChange={handleChange} />
      </label>
      <div>{errors.password}</div>
      <label htmlFor="confirmPassword">
        confirmPassword:
        <input name="confirmPassword" type="password" onChange={handleChange} />
      </label>
      <div>{errors.confirmPassword}</div>
      <button type="submit">Submit</button>
    </form>
  )
}

export const FormikDemo = () => {
  const initialValues = { email: '', password: '', confirmPassword: '' }
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      component={FormInner}
      onSubmit={values => {
        console.log(JSON.stringify(values))
      }}
    />
  )
}
