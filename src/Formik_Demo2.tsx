import React from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import * as Yup from 'yup'

const SignupSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, 'Too Short!')
    .max(70, 'Too Long!')
    .required('Required'),
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
})

export const FormikDemo2 = () => (
  <div>
    <Formik
      initialValues={{
        name: '',
        email: '',
      }}
      validationSchema={SignupSchema}
      onSubmit={values => {
        alert(JSON.stringify(values))
      }}
    >
      {() => (
        <Form>
          <label htmlFor="name2"> Name:</label>
          <Field id="name2" name="name" />
          <ErrorMessage name="name" />
          <br />
          <label htmlFor="email2">Email: </label>
          <Field id="email2" name="email" type="email" />
          <ErrorMessage name="email" />
          <br />
          <button type="submit">Submit</button>
        </Form>
      )}
    </Formik>
  </div>
)
